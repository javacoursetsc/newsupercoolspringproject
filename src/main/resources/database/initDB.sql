create table if not exists books(
  id int primary key,
  name varchar(100) not null,
  author varchar(100) not null,
  description varchar(1000) not null,
  price int not null
);