package ru.arubtsova.petproject.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.arubtsova.petproject.model.Book;
import ru.arubtsova.petproject.service.BookService;

import javax.validation.Valid;
import java.util.List;

@Api(
        value = "books",
        tags = "API книжный магазин")
@RestController
@RequestMapping("books/")
public class BookRestController {

    @Autowired
    BookService bookService;

    @ApiOperation(value = "Получить информацию о книге", notes = "Получить всю информацию о книге по id")
    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Book> getBook(@PathVariable("id") Integer bookId) {
        if (bookId == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Book book = bookService.getById(bookId);

        if (book == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(book, HttpStatus.OK);
    }

    @ApiOperation(value = "Добавить книгу", notes = "Создать книги")
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Book> saveBook(@RequestBody @Valid Book book) {
        if (book == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        HttpHeaders httpHeaders = new HttpHeaders();
        bookService.save(book);

        return new ResponseEntity<>(book, httpHeaders, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Обновить книгу", notes = "Обновить информацию о книге")
    @RequestMapping(value = "", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Book> updateBook(@RequestBody @Valid Book book, UriComponentsBuilder builder) {
        if (book == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        HttpHeaders httpHeaders = new HttpHeaders();
        Book bookToUpdate = bookService.getById(book.getId());
        if (bookToUpdate == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        bookService.update(bookToUpdate, book);

        return new ResponseEntity<>(book, httpHeaders, HttpStatus.OK);
    }

    @ApiOperation(value = "Удалить книгу", notes = "Удалить книгу")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Book> deleteBook(@PathVariable("id") Integer bookId) {
        if (bookId == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Book book = bookService.getById(bookId);

        if (book == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        bookService.delete(book);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @ApiOperation(value = "Получить список всех книг", notes = "Получить список всех книг с информацией о них")
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Book>> getAllBooks() {

        List<Book> books = bookService.getAll();

        if (books.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Получить список всех книг доступных к покупке/продаже", notes = "Получить список всех книг доступных к покупке/продаже")
    @RequestMapping(value = "canBuy/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Book>> getAllAvailableBooks() {

        List<Book> books = bookService.getAllAvailable();

        if (books.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Заказать книги", notes = "Заказать книги с сайта book24 по автору")
    @RequestMapping(value = "order/{author}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Book>> orderNewBooks(@PathVariable("author") String author) {
        if (author == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        return new ResponseEntity<List<Book>>(bookService.orderNewBooks(author), HttpStatus.OK);
    }

}
