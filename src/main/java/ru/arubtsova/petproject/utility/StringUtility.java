package ru.arubtsova.petproject.utility;

public class StringUtility {

    public static String toUpperCase(String str) {

        String[] strArray = str.split(" ");

        for (int i = 0; i < strArray.length; i++) {
            strArray[i] = Character.toUpperCase(strArray[i].charAt(0)) + strArray[i].substring(1);
        }

        return String.join(" ", strArray);
    }

}
