package ru.arubtsova.petproject.service;

import ru.arubtsova.petproject.model.Book;

import java.util.List;

public interface BookServiceInterface {

    Book getById(Integer id);

    void save(Book book);

    void delete(Book book);

    List<Book> getAll();

    void update(Book bookToUpdate, Book book);

    List<Book> getAllAvailable();

    List<Book> orderNewBooks(String author);

}
