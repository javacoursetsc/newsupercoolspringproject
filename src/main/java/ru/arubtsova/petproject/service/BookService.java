package ru.arubtsova.petproject.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.arubtsova.petproject.model.Book;
import ru.arubtsova.petproject.repository.BookRepository;
import ru.arubtsova.petproject.utility.OrderUtility;
import ru.arubtsova.petproject.utility.StringUtility;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BookService implements BookServiceInterface {

    @Autowired
    BookRepository bookRepository;

    @Override
    public Book getById(Integer id) {
        log.info("Ищем книгу по id " + id);
        return bookRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Book book) {
        book.setAuthor(StringUtility.toUpperCase(book.getAuthor()));
        log.info("Сохраняем книгу  " + book.getName() + " " + book.getAuthor());
        bookRepository.save(book);
    }

    @Override
    public void delete(Book book) {
        log.info("Удаляем книгу  " + book.getName() + " " + book.getAuthor());
        bookRepository.delete(book);
    }

    @Override
    public List<Book> getAll() {
        log.info("Ищем все книги");
        return bookRepository.findAll();
    }

    @Override
    public void update(Book bookToUpdate, Book book) {
        log.info("Обновляем книгу с id " + bookToUpdate.getId());
        bookToUpdate.setName(book.getName());
        bookToUpdate.setAuthor(StringUtility.toUpperCase(book.getAuthor()));
        bookToUpdate.setDescription(book.getDescription());
        bookToUpdate.setPrice(book.getPrice());
        bookToUpdate.setAvailable(book.getAvailable());
        bookRepository.save(bookToUpdate);
    }

    @Override
    public List<Book> getAllAvailable() {
        log.info("Ищем все доступные к продаже книги");
        return bookRepository.findAll().stream().filter(b -> b.getAvailable()).collect(Collectors.toList());
    }

    @Override
    public List<Book> orderNewBooks(String author) {
        String authorFormatted = StringUtility.toUpperCase(author);

        List<String> booksToOrder = OrderUtility.sendRequest(authorFormatted.replace("%20", " "));
        for (String bookName : booksToOrder) {
            Book book = new Book();
            book.setName(bookName);
            book.setAuthor(authorFormatted);
            book.setDescription("Описание будет добавлено позднее");
            book.setPrice(0);
            book.setAvailable(false);
            save(book);
        }

        return bookRepository.findAll().stream().filter(b -> Objects.equals(b.getAuthor(), authorFormatted) && b.getPrice() == 0).collect(Collectors.toList());
    }

}
