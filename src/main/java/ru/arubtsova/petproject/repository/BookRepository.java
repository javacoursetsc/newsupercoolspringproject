package ru.arubtsova.petproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.arubtsova.petproject.model.Book;

public interface BookRepository extends JpaRepository<Book, Integer> {
}
